package au.com.endeavour.whra.constants;

public class Constants {

	public static final int RESPONSE_CODE_SUCCESS = 200;
	public static final int RESPONSE_CODE_INTERNAL_ERROR = 500;
	public static final String RESPONSE_MESSAGE_SUCCESS = "Success";
	public static final String RESPONSE_MESSAGE_ERROR = "Error: ";
	public static final String INPUT_PARAMETER_EMPTY = "Input parameter is empty";
	

}
