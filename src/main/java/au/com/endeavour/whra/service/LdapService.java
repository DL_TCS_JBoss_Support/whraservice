package au.com.endeavour.whra.service;

import org.springframework.stereotype.Service;

import au.com.endeavour.whra.dto.UserDTO;

@Service
public interface LdapService {

	 public UserDTO getUserDetails(String username);
}
