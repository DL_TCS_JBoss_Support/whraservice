package au.com.endeavour.whra.service;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.endeavour.whra.dto.UserDTO;


/**
 * UserService Implementation
 *
 * @author JavaChap
 */
@Service
public class LdapServiceImpl implements LdapService {


	@Autowired
	LdapTemplate ldapTemplate;

	@Override
	public UserDTO getUserDetails(String username) {
		LdapQuery qry = query()
				.base("OU=Users,OU=Accounts")
				.attributes("mail", "displayName", "sAMAccountName", "employeeID")
				.where("objectclass").is("person")
				.and("sAMAccountName").is(username);
		List<UserDTO> persons = ldapTemplate.search(qry, new LdapPersonAttributesMapper());
		
		return (persons.isEmpty()) ? null : persons.get(0);
	}

	private class LdapPersonAttributesMapper implements AttributesMapper<UserDTO> {
		@Override
		public UserDTO mapFromAttributes(Attributes attributes) throws NamingException {
			UserDTO p = new UserDTO();
			if (attributes.get("mail") != null) p.setEmail((String) attributes.get("mail").get());
			if (attributes.get("displayName") != null) p.setGivenName((String) attributes.get("displayName").get());
			if (attributes.get("sAMAccountName") != null) p.setUserName(((String) attributes.get("sAMAccountName").get()).toLowerCase());
			if (attributes.get("employeeID") != null) p.setServiceNumber((String) attributes.get("employeeID").get());
			return p;
		}
	}

}