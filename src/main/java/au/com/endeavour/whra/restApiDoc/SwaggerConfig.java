package au.com.endeavour.whra.restApiDoc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	//http://localhost:8080/WHRAService-1.0.0/swagger-ui.html
	//http://127.0.0.1:8080/WHRAService-1.0.0/swagger-ui.html
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()                 .apis(RequestHandlerSelectors.basePackage("au.com.endeavour.whra"))
                .paths(PathSelectors.any()) 
                .build();
             
    }
}