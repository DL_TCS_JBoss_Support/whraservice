package au.com.endeavour.whra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EUSER")
public class User extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4601281348699403062L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "USR_SEQUENCE")
	@Column(name = "ID")
	private long id;

	@Column(name = "LOGIN_NAME")
	private String loginName;

	@Column(name = "SERVICE_NUMBER")
	private String serviceNumber;

	@Column(name = "NAME")
	private String name;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "ACTIVE")
	private int active;
	
	@Column(name = "CONSOLE_FLEET_UPLOAD")
	private boolean consoleFleetUpload;
	
	public User(){}
	
	public User(String loginName, String serviceNumber, String name, String email, int active){
		this.loginName = loginName;
		this.serviceNumber = serviceNumber;
		this.name = name;
		this.email = email;
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public boolean isConsoleFleetUpload() {
		return consoleFleetUpload;
	}

	public void setConsoleFleetUpload(boolean consoleFleetUpload) {
		this.consoleFleetUpload = consoleFleetUpload;
	}

}
