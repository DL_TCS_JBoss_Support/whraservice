package au.com.endeavour.whra.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.NONE)
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class BaseModel implements Serializable {

	@Version
	@Column(name = "VERSION")
	protected Integer version;

	@Column(name = "CREATE_DATE")
	protected Date createdDate;

	@Column(name = "CREATE_USER")
	protected Integer createdUser;

	@Column(name = "MODIFIED_DATE")
	protected Date modifiedDate;

	@Column(name = "MODIFIED_USER")
	protected Integer modifiedUser;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Integer createdUser) {
		this.createdUser = createdUser;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(Integer modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

}
