package au.com.endeavour.whra.controller;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import au.com.endeavour.whra.model.User;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface WhraRepository extends PagingAndSortingRepository<User, Long> {

	List<User> findByLoginName(@Param("name") String name);

}