package au.com.endeavour.whra.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.com.endeavour.whra.constants.Constants;
import au.com.endeavour.whra.dto.UserDTO;
import au.com.endeavour.whra.dto.WhraResponse;
import au.com.endeavour.whra.service.LdapService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/whra")
@Api(tags = {"whra"})
public class LoginController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	LdapService ldapService;

	
	//http://127.0.0.1:8080/WHRAService-1.0.0/whra/getUserDetails
	 @RequestMapping(value = "getUserDetails",headers = "Accept=application/json",
     method = RequestMethod.POST,
     consumes = {"application/json"},
     produces = {"application/json"})
	 @ApiOperation(value = "fetch user details from ldap and db",
	 notes = "Return all details related to users like authenticated, email , employeeNumber,givenName ,userName,password,serviceNumber" )
	public  WhraResponse  getUserDetails(@RequestBody(required = true) String username) {

		WhraResponse response = new WhraResponse();

		try {
			if(username!=null) {
				
				//TODO:First check in Database if details is available for the user.
				//Will check in LDAP if database query return no results for the user.
				UserDTO userDTOResponse = ldapService.getUserDetails(username);

				response.setResult(userDTOResponse);
				response.setStatus(Constants.RESPONSE_CODE_SUCCESS);
				response.setMessage(Constants.RESPONSE_MESSAGE_SUCCESS);
			}
			else {
				response.setStatus(Constants.RESPONSE_CODE_INTERNAL_ERROR);
				response.setMessage(Constants.RESPONSE_MESSAGE_ERROR + Constants.INPUT_PARAMETER_EMPTY);
			}

		} catch (Exception ex) {

			LOG.error(ex.getMessage());
			response.setStatus(Constants.RESPONSE_CODE_INTERNAL_ERROR);
			response.setMessage(Constants.RESPONSE_MESSAGE_ERROR + ex.getMessage());

		}
		return response;
	}
	 
	/* //http://127.0.0.1:8080/WHRAService-1.0.0/whra/example/v1/hotels
	 @RequestMapping(value = "/example/v1/hotels",
	            method = RequestMethod.GET,
	            produces = {"application/json", "application/xml"})
	    @ResponseStatus(HttpStatus.OK)
	    @ApiOperation(value = "Get a paginated list of all hotels.", notes = "The list is paginated. You can provide a page number (default 0) and a page size (default 100)")
	    public @ResponseBody String getAllHotel(@ApiParam(value = "userName ", required = true)
	                                      @RequestParam(value = "userName", required = true, defaultValue = "10") String userName,
	                                      HttpServletRequest request, HttpServletResponse response) {
	        return "tested ";
	    }*/

}
