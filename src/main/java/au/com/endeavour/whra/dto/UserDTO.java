package au.com.endeavour.whra.dto;

public class UserDTO {

	private boolean authenticated;
	private String email;
	private String employeeNumber;
	private String givenName;
	private String userName;
	private String password;
	private String serviceNumber;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName){
		this.userName = userName;

	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployeeNumber() {

		if(employeeNumber==null || employeeNumber == "")
			return "0";
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}



}
