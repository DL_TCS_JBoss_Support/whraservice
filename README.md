# WHRAService 

Sample "Spring Boot" starter app to demonstrate how to integrate the following:

* Connection to Oracle database, using JDBC, JPA, Hibernate, HikariCP and Spring Data REST.
* SMTP server for email delivery (via Spring's JavaMail API)
* MS Active Directory via LDAP (via Spring's LDAP integration components)

These integrations should be specific to Endeavour Energy, using host/IP's and credentials to demonstrate correct access.

# Building via Maven

This sample project can be built using Maven at a command prompt. Execute:

* git clone https://bitbucket.org/eemobility/whra-sample-backend.git
* cd whra-sample-backend/
* mvn clean package

A WAR file should be created in the "target" directory - deployable to a Java application server (such as JBoss EAP, or Wildfly).


## Fixing Maven Build Errors

If the maven build fails due to an error resolving the Oracle JDBC driver dependency, you must follow the instructions in this blog post:

* https://blogs.oracle.com/dev2dev/get-oracle-jdbc-drivers-and-ucp-from-oracle-maven-repository-without-ides


# Running the whra-sample-backend app

Once deployed to an app server, the app can be tested by making a request to the following URL:

* In a Browser: http://127.0.0.1:8080/whra-sample-0.1.0
* Using cURL: curl http://127.0.0.1:8080/whra-sample-0.1.0


URL endpoints to test Oracle DB integration (this connects to the Oracle DEV database for HVNL):

* http://127.0.0.1:8080/whra-sample-0.1.0/users
* http://127.0.0.1:8080/whra-sample-0.1.0/users/62
* http://127.0.0.1:8080/whra-sample-0.1.0/users/search/findByLoginName?name=thakan


URL endpoints to test SMTP mail integration:

* http://127.0.0.1:8080/whra-sample-0.1.0/test/mail/{username}

(Any valid Endeavour Energy username should work)


URL endpoints to test AD/LDAP integration:

* http://127.0.0.1:8080/whra-sample-0.1.0/test/ldap
* http://127.0.0.1:8080/whra-sample-0.1.0/test/ldap/thakan
* http://127.0.0.1:8080/whra-sample-0.1.0/test/ldap/{username}, etc.

(Any valid Endeavour Energy username should work)


# Spring Boot Actuator Endpoints

This sample application includes the Spring Boot Actuator components. These allow "production-ready" features to be included
automatically into the build.

This exposes several URL endpoints that allow interrogation of the running app for monitoring or analytics purposes.

All exposed actuator URL endpoints can be viewed at the following URL:

* http://127.0.0.1:8080/whra-sample-0.1.0/actuator

A most useful URL for monitoring purposes:

* http://127.0.0.1:8080/whra-sample-0.1.0/health

*http://127.0.0.1:8080/whra-sample-new-0.1.0/test/ldap/thakan


